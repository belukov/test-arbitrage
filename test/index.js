'use strict';

const assert = require('assert');

const profits = require('../lib/profit').profits;

function order(rate, amount) {
  return {rate, amount};
}

/**
* Вспомогательная функция для проверки результата вычислений
*
* @param array - Результат вычисления
* @param array - Массив с ожидаемыми результатами вида [<за сколько купили>, <за сколько продали>, <кол-во купленной валюты>]
*/
function assertResults(result, expect) {

  assert(Array.isArray(result), "Rsult is not an array");
  assert.equal(result.length, expect.length);
  for (let i = 0; i < expect.length; i++) {
  
    let [buy, sell, amount] = expect[i];
    let percent = ((sell - buy) / buy * 100).toFixed(4);

    assert.equal(result[i].amount, amount);
    assert.equal(result[i].percent, percent);
  }
}

describe("Testing prfit lib", () => {

  it("One ask, one bid", () => {
  
    const asks = [
      order(10000, 2)
    ];
    const bids = [
      order(15000, 1.5)
    ];
    let result = profits(asks, bids);

    assertResults(result, [
      [10000, 15000, 1.5]
    ]);
  });

  it("Must split one bid for two best asks", () => {
  
    const asks = [
      order(10000, 1),
      order(11000, 1),
      order(12000, 1)
    ];
    const bids = [
      order(15000, 1.5)
    ];
    let result = profits(asks, bids);

    assertResults(result, [
      [10000, 15000, 1],
      [11000, 15000, 0.5]
    ]);
  });

  it("Must use one ask for several bids", () => {
    const asks = [
      order(10000, 5),
      order(11000, 1)
    ];

    const bids = [
      order(15000, 1),
      order(14000, 6),
      order(13000, 2)
    ];

    let result = profits(asks, bids);

    assertResults(result, [
      [10000, 15000, 1],
      [10000, 14000, 4],
      [11000, 14000, 1]
    ]);
  });

  it("Must return empty result for unprofitable offers", () => {
  
    const asks = [
      order(15500, 10),
      order(16000, 1)
    ];
    const bids = [
      order(15000, 1),
      order(15100, 1)
    ];
    let result = profits(asks, bids);

    assertResults(result, []);
    //assert.equal(0, result.length);

  });

  it("Must return correct result if arguments is empty", () => {
  
    const asks = [];
    const bids = null;
    let result = profits(asks, bids);

    assertResults(result, []);
  });

  it("5 asks, 6 bids from example. Must provide better opportunities", () => {
  
    const asks = [
      order(14500, 2),
      order(15000, 1.2),
      order(15500, 1.5),
      order(15700, 0.5),
      order(16000, 1)
    ];
    const bids = [
      order(15600, 1.5),
      order(15400, 1),
      order(14900, 2),
      order(14300, 4),
      order(1400, 1),
      order(13900, 5)
    ];

    let result = profits(asks, bids);

    assertResults(result, [
      [14500, 15600, 1.5],
      [14500, 15400, 0.5],
      [15000, 15400, 0.5]
    ]);
  });
});
