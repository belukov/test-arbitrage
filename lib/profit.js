'use strict';

function* OrderGenerator(orders) {

  for(let i = 0; i < orders.length; i++) {
    yield {...orders[i]};
  }
  return null;
}

function profits(asks = [], bids = []) {

  let result = [];

  if (!asks.length || !bids.length) return result;

  let askGen = OrderGenerator(asks);
  let bidGen = OrderGenerator(bids);

  let ask = askGen.next().value;
  let bid = bidGen.next().value;
  
  while (ask && bid) {
 
    if (ask.rate >= bid.rate) break;

    let amount = ask.amount < bid.amount ? ask.amount : bid.amount;
    let percent = ((bid.rate - ask.rate) / ask.rate * 100).toFixed(4);
    result.push({amount, percent});

    ask.amount -= amount;
    bid.amount -= amount;

    if (ask.amount <= 0) {
      ask = askGen.next().value;
    }
    if (bid.amount <= 0) {
      bid = bidGen.next().value;
    }
  }
  return result;
}

module.exports = {profits};
